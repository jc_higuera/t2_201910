package test;

import model.data_structures.LinkedList;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class Data_StructuresTest 
{

	private LinkedList<String> linkedList;

	@Before
	public void setup1()
	{
		linkedList = new LinkedList<>();
	}

	@Test
	public void testAdd()
	{
		setup1();
		linkedList.addAtEnd("a");
		try {
			linkedList.addAtK(1, "b");
			linkedList.addFirst("c");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(3, (int)linkedList.getSize());

	}

	@Test
	public void testDelete()
	{
		setup1();
		linkedList.addAtEnd("a");
		linkedList.addAtEnd("b");
		linkedList.addAtEnd("c");
		linkedList.delete();
		linkedList.deleteAtK(0);
		assertEquals(1, (int)linkedList.getSize());
		assertEquals("b", linkedList.getCurrentElement());
	}

	@Test
	public void testGetElement()
	{
		setup1();
		linkedList.addAtEnd("a");
		linkedList.addAtEnd("b");
		linkedList.addAtEnd("c");
		assertEquals("c", linkedList.getCurrentElement());
		try {
			assertEquals("b", linkedList.getElement(1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
