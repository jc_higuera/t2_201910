package controller;

import api.IMovingViolationsManager;
import model.data_structures.LinkedList;
import model.data_structures.MovingViolations;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IMovingViolationsManager  manager = new MovingViolationsManager();

	public static void loadMovingViolations() {
		manager.loadMovingViolations("./data/Moving_Violations_Issued_in_January_2018.csv");

	}

	public static LinkedList <MovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		return manager.getMovingViolationsByViolationCode(violationCode);
	}

	public static LinkedList <MovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		return manager.getMovingViolationsByAccident(accidentIndicator);
	}
}
