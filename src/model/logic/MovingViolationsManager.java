package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;
import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;
import model.data_structures.MovingViolations;

public class MovingViolationsManager implements IMovingViolationsManager {

	private LinkedList<MovingViolations> linkedList = new LinkedList<>();

	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub
		try {
			CSVReader reader = new CSVReader(new FileReader(movingViolationsFile));
			String [] nextLine;
			nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
				// nextLine[] is an array of values from the line
				linkedList.addAtEnd(new MovingViolations(nextLine[0], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], nextLine[8], nextLine[9], nextLine[10], nextLine[12], nextLine[13], nextLine[14], nextLine[15], nextLine[16]));

			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(linkedList.getSize());

	}


	@Override
	public LinkedList<MovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList<MovingViolations> ans = new LinkedList<>();
		for (MovingViolations movingViolations : linkedList) 
		{
			if(movingViolations.getViolationCode().equals(violationCode)) {
				ans.addAtEnd(movingViolations);
			}
		}
		return ans;
	}

	@Override
	public LinkedList <MovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		LinkedList<MovingViolations> ans = new LinkedList<>();
		for (MovingViolations movingViolations : linkedList) 
		{
			if(movingViolations.getAccident().equals(accidentIndicator)) {
				ans.addAtEnd(movingViolations);
			}
		}
		return ans;
	}	


}
