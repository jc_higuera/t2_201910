package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int id;
	private int row;
	private String location;
	private int adressId;
	private String streetSegId;
	private String xCoord;
	private String yCoord;
	private String ticketType;
	private int fine;
	private int penalty1;
	private int penalty2;
	private String accident;
	private int agencyId;
	private String issueDate;
	private int violationCode;
	private String violationDesc;
	private int rowId;
	public VOMovingViolations(int id, int row, String location, int adressId, String streetSegId, String xCoord, String yCoord,
			String ticketType, int fine, int penalty1, int penalty2, String accident, int agencyId, String issueDate,
			int violationCode, String violationDesc, int rowId) 
	{
		super();
		this.id = id;
		this.row = row;
		this.location = location;
		this.adressId = adressId;
		this.streetSegId = streetSegId;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.ticketType = ticketType;
		this.fine = fine;
		this.penalty1 = penalty1;
		this.penalty2 = penalty2;
		this.accident = accident;
		this.agencyId = agencyId;
		this.issueDate = issueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
		this.rowId = rowId;
	}
	public int getId() {
		return id;
	}
	public int getRow() {
		return row;
	}
	public String getLocation() {
		return location;
	}
	public int getAdressId() {
		return adressId;
	}
	public String getStreetSegId() {
		return streetSegId;
	}
	public String getxCoord() {
		return xCoord;
	}
	public String getyCoord() {
		return yCoord;
	}
	public String getTicketType() {
		return ticketType;
	}
	public int getFine() {
		return fine;
	}
	public int getPenalty1() {
		return penalty1;
	}
	public int getPenalty2() {
		return penalty2;
	}
	public int getAgencyId() {
		return agencyId;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public int getViolationCode() {
		return violationCode;
	}
	public String getViolationDesc() {
		return violationDesc;
	}
	public int getRowId() {
		return rowId;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return 0;
	}	


	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return "";
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return getFine() + getPenalty1() + getPenalty2();
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accident;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDesc;
	}
}
