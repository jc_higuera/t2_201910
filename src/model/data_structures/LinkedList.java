package model.data_structures;

import java.util.Iterator;


public class LinkedList <T> implements ILinkedList<T>
{

	private Node<T> list;
	private int listSize;
	public LinkedList()
	{
		list = null;
	}

	public void addFirst(T item)
	{
		if(list!=null) {
			Node <T> newHead = new Node<> (item);
			newHead.setNext(list);
			list = newHead;
			list.setPrevious(newHead);
			listSize++;
		}else {
			list = new Node<>(item);
		}
	}

	public void addAtEnd(T item) 
	{
		Node <T> newNode = new Node<> (item);
		if(list == null) {
			list = newNode; 
		}
		else {
			Node <T> actual = list; 
			while(actual.getNext() != null) {
				actual = actual.getNext();
			}
			actual.setNext(newNode);
			newNode.setPrevious(actual);
		} listSize++;
	}

	public void addAtK(int k, T item) throws java.lang.Exception
	{
		Node <T> newNode = new Node<> (item);
		if(k>listSize||k<0) {
			throw new Exception("K is not valid");
		}
		else if(k==0) {
			list = newNode;
		}else {
			Node <T> actual = list; 
			int i = 1;
			while(k>i) {
				actual=actual.getNext();
				i++;
			}
			actual.setNext(newNode);
			newNode.setPrevious(actual);
		}
		listSize++;
	}

	public Node<T> getElement(int k) throws java.lang.Exception
	{
		if(k>listSize||k<0) 
		{
			throw new Exception("K is not valid");
		}
		else if(list == null) {
			return null;
		}
		else if(k==0)
		{
			return list;
		}
		else 
		{
			Node <T> actual = list; 
			int i = 1;
			while(k>i) {
				actual=actual.getNext();
				i++;
			}
			return actual.getNext();
		}
	}

	public Node<T> getCurrentElement()
	{
		if(list==null) {
			return null;
		}
		else {
			Node <T> actual = list; 
			while(actual.getNext() != null) {
				actual = actual.getNext();
			}
			return actual;
		}
	}

	public void delete()
	{
		if(list.getNext()==null) {
			list = null;
		}else {
			Node <T> actual = list; 
			Node <T> aEliminar = list.getNext();
			while(actual.getNext().getNext() != null) {
				actual = actual.getNext();
				aEliminar = actual.getNext().getNext();
			}
			actual.setNext(null);
			aEliminar.setPrevious(null);
		}
	}

	public void deleteAtK(int k)
	{
		if(k==0) {
			Node<T> aEliminar = list;
			list = aEliminar.getNext();
			list.setPrevious(null);
			aEliminar.setNext(null);
		}else {
			Node <T> actual = list; 
			int i = 1;
			while(k>i) {
				actual=actual.getNext();
				i++;
			}
			Node<T> aEliminar = actual.getNext();
			actual.setNext(aEliminar.getNext());
			actual.getNext().setPrevious(actual);
			aEliminar.setNext(null);
			aEliminar.setPrevious(null);
		}
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> iterator = new Iterator<T>() {
			Node<T> act = null;
			public boolean hasNext() {
				if (listSize == 0) {
					return false;
				}
				else {
					if (act == null) {
						return true;
					}else {
						return act.getNext() != null;
					}
				}
			}
			public T next() {
				if (act == null) {
					act = list;
				}else {
					act = act.getNext();
				}
				return act.getItem();

			}
			public boolean hasPrevious() {
				if (listSize == 0) {
					return false;
				}
				else {
					if (act == null) {
						return true;
					}else {
						return act.getPrevious() != null;
					}
				}
			}
			public T previous()
			{
				if (act == null) {
					act = list;
				}else {
					act = act.getPrevious();
				}
				return act.getItem();
			}
		};
		return iterator;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return listSize;
	}

}
